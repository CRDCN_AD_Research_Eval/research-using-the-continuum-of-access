clear
import delimited "E:\Downloads\cchs-82M0013-E-2014-Annual-component\cchs-82M0013-E-2014-Annual-component_F1.csv"

*create normalized weights
summarize wts_m
gen wtsn=(`r(N)'*wts_m)/`r(sum)'
gen wtsm=round(wts_m)

*show the sample
tab geogprv
tab sdcfimm

*select the immigrant Ontario sample
keep if sdcfimm==1 & geogprv==35
keep if dhhgage>0

*create the summary statistics - immigration then sex
tab sdcgres [fweight=wtsm]
tab dhh_sex [fweight=wtsm]

gen irregular=1 if den_132>1 & den_132<=7
replace irregular=0 if den_132==1

replace dhh_sex=(dhh_sex-2)*(-1)
replace sdcgres=(sdcgres-2)*(-1)

logit irregular dhh_sex [pweight=wtsn], or
logit irregular sdcgres [pweight=wtsn], or

*create the controls for the adjusted OR
gen agegr=1 if dhhgage==1 | dhhgage==2
replace agegr=2 if dhhgage==3 | dhhgage==4 | dhhgage==5 | dhhgage==6
replace agegr=3 if dhhgage==7 | dhhgage==8 | dhhgage==9 | dhhgage==10
replace agegr=4 if agegr==.

gen ms=1 if dhhgms<3
replace ms=2 if dhhgms>2 & dhhgms<9

gen edu=1 if edudr04==1
replace edu=2 if edudr04==2 | edudr04==3
replace edu=3 if edudr04==4

gen inc=1 if incghh==1
replace inc=2 if incghh==2 | incghh==3 | incghh==4
replace inc=3 if inc==.

gen lfs=1 if lbsdwss==1 | lbsdwss==2
replace lfs=2 if lfs~=1

gen smk=2 if smk_202==3
replace smk=1 if smk_202<3

gen alc=1 if alc_1==2
replace alc=2 if alc_2==1 | alc_2==2
replace alc=2 if alc_2==3 | alc_2==4 | alc_2==5 | alc_2==6 | alc_2==7

gen teeth=1 if oh1_20==1 | oh1_20==2
replace teeth=2 if oh1_20==3
replace teeth=3 if oh1_20==4 | oh1_20==5

gen health=1 if gen_01==1 | gen_01==2
replace health=2 if gen_01==3
replace health=3 if gen_01==4 | gen_01==5

gen stress=1 if gen_07==1 | gen_07==2 | gen_07==3
replace stress=2 if gen_07==4 | gen_07==5

*now run the regression - note that reference categories differ here (not necessary for the reproduction). The only valid results comparison to the original paper are sex, and years since immigration

logit irregular i.sdcgres i.dhh_sex i.agegr i.ms i.edu i.inc i.lfs i.smk i.alc i.teeth i.health i.stress [pweight=wtsn],or
