data work.Step_1;
  Set RTRAdata.CCHS (keep = GEO_PRV SDCFIMM DEN_132 SDCDRES DHH_SEX ID);
  /* Set max length for derived variables. */
  length Visits $ 9;
  length Status $ 16;
  length Sex $ 7;

  /* Limit data and subset population to create sample */
  if GEO_PRV = 35;

  if SDCFIMM = 1;

  /* Set default value as invalid when other conditions not met */
  Visits = "Invalid";

  /* Define dental visits as regular or irregular */
  if DEN_132 = 1 then do;
    Visits = "Regular";
  end;

  if DEN_132 = 2
    or DEN_132 = 3
    or DEN_132 = 4
    or DEN_132 = 5
    or DEN_132 = 6
    or DEN_132 = 7 then do;
    Visits = "Irregular";
  end;

  /* Years since immigration = status, setting default value of unknown*/
  Status = "Unknown";

  /* regrouping years since immigration to define status = recent, established, very_established */
  if SDCDRES <= 9 then do;
    Status = "Recent";
  end;

  if SDCDRES > 9
    and SDCDRES <= 20 then do;
    Status = "Established";
  end;

  if SDCDRES > 20 then do;
    Status = "Very_Established";
  end;

  /* Setting default value for Sex when other conditions not met */
  Sex = "Unknown";

  /* Regrouping sex variable*/
  if DHH_SEX = 1 then do;
    Sex = "Male";
  end;

  if DHH_SEX = 2 then do;
    Sex = "Female";
  end;

run;

/* Run RTRA proportion macro = Sex, Status, by Dental Visits */
%RTRAProportion(
  InputDataset = work.Step_1,
  OutputName = imm_dent_prop,
  ClassVarList = Sex Status,
  ByVar = Visits,
  UserWeight = WTS_M);
