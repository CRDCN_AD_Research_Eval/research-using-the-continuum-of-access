**Slides and accompanying code for the Continuum of access presentation at DLI 2021 (Nov)**

**Contents**

DLI COA academic research.pptx : Powerpoint slides with audio and video demonstrations

DLI COA academic research - no audio.pptx : Powerpoint slides with video demonstrations only

DLI presentation.do : Code to analyze the public use microdata to reproduce the original paper

DLI presentation - sex proportions.sas : Code created by the SAS code-builder to create the sex proportions in the sample

DLI presentation - immigrant proportions.sas : Code created by the SAS code-builder to create the immigrant proportions in the sample

Data access statement.txt : Data access statement

Citation.txt : Citation for the paper conducted in the reproduction exercise in the presentation
